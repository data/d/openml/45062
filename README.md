# OpenML dataset: shrutime

https://www.openml.org/d/45062

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This data set contains details of a bank's customers and the target variable is a binary variable reflecting the fact whether the customer left the bank (closed his account) or he continues to be a customer.Source: https://www.kaggle.com/datasets/shrutimechlearn/churn-modelling

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45062) of an [OpenML dataset](https://www.openml.org/d/45062). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45062/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45062/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45062/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

